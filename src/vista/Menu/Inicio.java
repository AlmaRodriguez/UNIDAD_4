/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.Menu;

import modelo.usuarios.User_session;
import modelo.usuarios.query_login;
import vista.usuarios.CRUD_USUARIOS;

/**
 *
 * @author ALMA ROSA RODRIGUEZ SANTOS
 */
public class Inicio extends javax.swing.JFrame {
    
    User_session mod;
    public Inicio() {
        initComponents();
    }
    public Inicio(User_session mod) {
        initComponents();
        setLocationRelativeTo(null);
        this.mod = mod;
        switch (mod.getId_nivel()) {
                case 1:
                    menu_admin.setVisible(true);
                    menu_maestros.setVisible(false);
                    menu_estudiantes.setVisible(false);
                    break;
                case 2:
                    menu_admin.setVisible(false);
                    menu_maestros.setVisible(true);
                    menu_estudiantes.setVisible(false);
                    break;
                case 3:
                    menu_admin.setVisible(false);
                    menu_maestros.setVisible(false);
                    menu_estudiantes.setVisible(true);
                    break;
        }
        String name_user = "Bienvenido(a) "+mod.getNombre() +" "+ mod.getApellido_paterno() +" "+mod.getApellido_materno();
        label_user.setText(name_user);
        label_rol.setText("Nivel de Usuario: "+mod.getNivel());
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        label_user = new javax.swing.JLabel();
        label_rol = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        menu_file = new javax.swing.JMenu();
        item_about = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        menu_admin = new javax.swing.JMenu();
        menu_list_users = new javax.swing.JMenuItem();
        menu_list_maestros = new javax.swing.JMenuItem();
        menu_list_alumnos_1 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        menu_maestros = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        menu_estudiantes = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        label_user.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        label_rol.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        menu_file.setText("File");

        item_about.setText("About...");
        item_about.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                item_aboutActionPerformed(evt);
            }
        });
        menu_file.add(item_about);

        jMenuItem1.setText("Log Out");
        menu_file.add(jMenuItem1);

        jMenuBar1.add(menu_file);

        menu_admin.setText("Administrador");

        menu_list_users.setText("List Users");
        menu_list_users.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menu_list_usersActionPerformed(evt);
            }
        });
        menu_admin.add(menu_list_users);

        menu_list_maestros.setText("List Teachers");
        menu_admin.add(menu_list_maestros);

        menu_list_alumnos_1.setText("List Students");
        menu_admin.add(menu_list_alumnos_1);

        jMenuItem5.setText("List Subject");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        menu_admin.add(jMenuItem5);

        jMenuItem6.setText("List Groups");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        menu_admin.add(jMenuItem6);

        jMenuItem7.setText("List of Qualifications");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        menu_admin.add(jMenuItem7);

        jMenuItem11.setText("Ratings Log");
        menu_admin.add(jMenuItem11);

        jMenuBar1.add(menu_admin);

        menu_maestros.setText("Teachers");

        jMenuItem8.setText("List Subject");
        menu_maestros.add(jMenuItem8);

        jMenuItem9.setText("List Students");
        menu_maestros.add(jMenuItem9);

        jMenuItem2.setText("List Groups");
        menu_maestros.add(jMenuItem2);

        jMenuItem4.setText("List of Qualifications");
        menu_maestros.add(jMenuItem4);

        jMenuBar1.add(menu_maestros);

        menu_estudiantes.setText("Students");

        jMenuItem3.setText("List Subject");
        menu_estudiantes.add(jMenuItem3);

        jMenuItem10.setText("List of Qualifications");
        menu_estudiantes.add(jMenuItem10);

        jMenuBar1.add(menu_estudiantes);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(label_user, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label_rol, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(452, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(label_user, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(label_rol, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(261, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void menu_list_usersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menu_list_usersActionPerformed
        CRUD_USUARIOS ventana = new CRUD_USUARIOS();
        ventana.setVisible(true);
    }//GEN-LAST:event_menu_list_usersActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed

    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        ABOUT ventana = new ABOUT();
        ventana.setVisible(true);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void item_aboutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_item_aboutActionPerformed
       ABOUT ventana = new ABOUT();
       ventana.setVisible(true);
    }//GEN-LAST:event_item_aboutActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem item_about;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JLabel label_rol;
    private javax.swing.JLabel label_user;
    private javax.swing.JMenu menu_admin;
    private javax.swing.JMenu menu_estudiantes;
    private javax.swing.JMenu menu_file;
    private javax.swing.JMenuItem menu_list_alumnos_1;
    private javax.swing.JMenuItem menu_list_maestros;
    private javax.swing.JMenuItem menu_list_users;
    private javax.swing.JMenu menu_maestros;
    // End of variables declaration//GEN-END:variables
}
