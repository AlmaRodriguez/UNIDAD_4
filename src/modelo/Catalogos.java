/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import modelo.usuarios.cat_nivel;

/**
 *
 * @author ALMA ROSA RODRIGUEZ SANTOS
 */
public class Catalogos extends Conexion{
    
    public ArrayList<cat_nivel> cat_niveles_lista(){
        ArrayList cat_niveles =  new ArrayList();
        cat_nivel catalogo;        
        try {
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection con = getConexion();

            String sql = "SELECT id_nivel, descripcion, activo FROM cat_nivel_usuarios WHERE activo = 1";
            ps = con.prepareStatement(sql);
            rs =  ps.executeQuery();            
            while(rs.next()){    
                catalogo = new cat_nivel();
                ResultSetMetaData result = rs.getMetaData();
                catalogo.setId_nivel(rs.getInt(1));
                catalogo.setNivel(rs.getString(2));
                cat_niveles.add(catalogo);
            }  
            rs.close();
            con.close();
        }catch (Exception e) {
        }    
        return cat_niveles;        
    }
    
}
