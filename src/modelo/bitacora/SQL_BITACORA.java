/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.bitacora;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Conexion;

/**
 *
 * @author CESAR CORDOVA CALDERON
 */
public class SQL_BITACORA extends Conexion{
    
    public ArrayList<BITACORA> bitacora_lista(){
        ArrayList bitacora_lista =  new ArrayList();
        BITACORA bita;        
        try {
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection con = getConexion();

            String sql = 
            "SELECT CONCAT_ws(nombre,apellido_paterno,apellido_materno,' ') AS maestro, bitacora_calificaciones.descripcion as accion_bitacora, "+
            "bitacora_calificaciones.fecha, cat_materias.descripcion as materia, cat_grupos.descripcion as grupo, cat_grupos.turno,"+
            "calificaciones.calificacion, CONCAT_ws(nombre,apellido_paterno,apellido_materno,' ') AS alumno FROM bitacora_calificaciones"+
            "INNER JOIN calificaciones ON bitacora_calificaciones.id_calificaciones = calificaciones.id_calificacion"+
            "INNER JOIN usuarios ON bitacora_calificaciones.id_user = usuarios.id_user"+
            "INNER JOIN cat_materias ON calificaciones.id_materia = cat_materias.id_materia"+
            "INNER JOIN r_grupo_usuarios ON calificaciones.id_user_grupo = r_grupo_usuarios.id_user_grupo AND r_grupo_usuarios.id_user = usuarios.id_user"+
            "INNER JOIN cat_grupos ON r_grupo_usuarios.id_grupo = cat_grupos.id_grupo";
            ps = con.prepareStatement(sql);
            rs =  ps.executeQuery();            
            while(rs.next()){    
                bita = new BITACORA();
                ResultSetMetaData result = rs.getMetaData();
                bita.setMaestro(rs.getString(1));
                bita.setAccion_bitacora(rs.getString(2));
                bita.setFecha(rs.getString(3));
                bita.setMateria(rs.getString(4));
                bita.setGrupo(rs.getString(5));
                bita.setTurno(rs.getString(6));
                bita.setCalificacion(rs.getInt(7));
                bita.setAlumno(rs.getString(8));
                bitacora_lista.add(bita);
            }  
            rs.close();
            con.close();
        }catch (Exception e) {
        }    
        return bitacora_lista;        
    }
    public boolean add_bitacora(BITACORA bitacora) throws SQLException{
        PreparedStatement ps = null;
        Connection con = getConexion();
        
        String sql = "IINSERT INTO bitacora_calificaciones (id_user,id_calificaciones,descripcion,fecha) VALUES (?, ?, ?, ?, ?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1,bitacora.getId_user());
            ps.setInt(2,bitacora.getId_calificacion());
            ps.setString(3,bitacora.getAccion_bitacora());
            ps.setString(4,bitacora.getFecha());
            ps.execute();
            return true;  
            
        }catch (SQLException ex) {
            System.err.println(ex);
            return false;
        } finally {
            try{
                con.close();
            }catch (SQLException ex) {
                System.err.println(ex);
            }
        }
        
    }
}
