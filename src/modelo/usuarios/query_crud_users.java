/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.usuarios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.*;

/**
 *
 * @author JULIAN VELAZQUEZ CAZARES
 */
public class query_crud_users extends Conexion{
    
    public ArrayList<Usuarios> lista_usuarios(){
        ArrayList lista_usuarios =  new ArrayList();
        Usuarios user;        
        try {
            PreparedStatement ps = null;
            ResultSet rs = null;
            Connection con = getConexion();

            String sql = "SELECT id_user, usuario, password, cat_nivel_usuarios.descripcion as nivel, nombre, apellido_paterno, apellido_materno,usuarios.id_nivel "+
            "FROM usuarios INNER JOIN cat_nivel_usuarios ON usuarios.id_nivel = "+
            "cat_nivel_usuarios.id_nivel WHERE usuarios.activo = 1";
            ps = con.prepareStatement(sql);
            rs =  ps.executeQuery();            
            while(rs.next()){    
                user = new Usuarios();
                ResultSetMetaData result = rs.getMetaData();
                user.setId_usuario(rs.getInt(1));
                user.setUsuario(rs.getString(2));
                user.setPassword(rs.getString(3));
                user.setNivel(rs.getString(4));
                user.setNombre(rs.getString(5));
                user.setApellido_paterno(rs.getString(6));
                user.setApellido_materno(rs.getString(7));
                user.setId_nivel(rs.getInt(8));
                lista_usuarios.add(user);
            }  
            rs.close();
            con.close();
        }catch (Exception e) {
        }    
        return lista_usuarios;        
    }
    public boolean add_user(Usuarios user) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();
        
        String sql = "INSERT INTO usuarios (usuario,password,id_nivel,nombre,apellido_paterno,apellido_materno) VALUES (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1,user.getUsuario());
            ps.setString(2,user.getPassword());
            ps.setInt(3,user.getId_nivel());
            ps.setString(4,user.getNombre());
            ps.setString(5,user.getApellido_paterno());
            ps.setString(6,user.getApellido_materno());
            ps.execute();
            return true;          
            
        }catch (SQLException ex) {
            Logger.getLogger(query_crud_users.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }     
    }
    public boolean update_user(Usuarios user) throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();
        
        String sql = "UPDATE usuarios SET id_nivel=?, nombre=?, apellido_paterno=?, apellido_materno=?  where id_user=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1,user.getId_nivel());
            ps.setString(2,user.getNombre());
            ps.setString(3,user.getApellido_paterno());
            ps.setString(4,user.getApellido_materno());
            ps.setInt(5,user.getId_usuario());
            ps.execute();
            return true;          
            
        }catch (SQLException ex) {
            Logger.getLogger(query_crud_users.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }     
    }
   
    
    
}
