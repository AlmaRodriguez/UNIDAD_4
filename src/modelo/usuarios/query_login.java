/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.usuarios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Conexion;

/**
 *
 * @author ALMA ROSA RODRIGUEZ SANTOS
 */
public class query_login extends Conexion{
    
    public boolean login(User_session user)
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();
        String sql = "SELECT id_user, usuario, password, cat_nivel_usuarios.id_nivel, nombre, apellido_paterno, apellido_materno,"+
        "cat_nivel_usuarios.descripcion as nivel FROM usuarios INNER JOIN cat_nivel_usuarios ON "+
        "usuarios.id_nivel = cat_nivel_usuarios.id_nivel WHERE usuarios.usuario =?";
        
        try 
        {
            ps = con.prepareStatement(sql);
            ps.setString(1, user.getUsuario());
            rs =  ps.executeQuery();
            
            if(rs.next())
            {
                if(user.getPassword().equals(rs.getString(3))){
                    user.setId_usuario(rs.getInt(1));
                    user.setUsuario(rs.getString(2));
                    user.setPassword(rs.getString(3));
                    user.setId_nivel(rs.getInt(4));
                    user.setNombre(rs.getString(5));
                    user.setApellido_paterno(rs.getString(6));
                    user.setApellido_materno(rs.getString(7));
                    user.setNivel(rs.getString(8));
                    return true;
                }else{
                    return false;
                }
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(query_login.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
