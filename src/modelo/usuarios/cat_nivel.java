
package modelo.usuarios;

/**
 *
 * @author JULIAN VELAZQUEZ CAZARES
 */
public class cat_nivel {
    
    private int id_nivel;
    private String nivel;

    public int getId_nivel() {
        return id_nivel;
    }

    public void setId_nivel(int id_nivel) {
        this.id_nivel = id_nivel;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }
    
    
    
}
