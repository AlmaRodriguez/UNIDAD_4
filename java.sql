/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : java

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-08-17 19:08:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for articulos
-- ----------------------------
DROP TABLE IF EXISTS `articulos`;
CREATE TABLE `articulos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of articulos
-- ----------------------------
INSERT INTO `articulos` VALUES ('1', 'alma', '121');

-- ----------------------------
-- Table structure for bitacora_calificaciones
-- ----------------------------
DROP TABLE IF EXISTS `bitacora_calificaciones`;
CREATE TABLE `bitacora_calificaciones` (
  `id_bitacora` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_calificaciones` int(11) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`id_bitacora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bitacora_calificaciones
-- ----------------------------

-- ----------------------------
-- Table structure for calificaciones
-- ----------------------------
DROP TABLE IF EXISTS `calificaciones`;
CREATE TABLE `calificaciones` (
  `id_calificacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_materia` int(11) DEFAULT NULL,
  `id_user_grupo` int(11) DEFAULT NULL,
  `calificacion` decimal(10,0) DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_calificacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of calificaciones
-- ----------------------------

-- ----------------------------
-- Table structure for cat_grupos
-- ----------------------------
DROP TABLE IF EXISTS `cat_grupos`;
CREATE TABLE `cat_grupos` (
  `id_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `turno` char(1) DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_grupo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_grupos
-- ----------------------------
INSERT INTO `cat_grupos` VALUES ('1', '1-A', '1', '1');
INSERT INTO `cat_grupos` VALUES ('2', '1-B', '1', '1');
INSERT INTO `cat_grupos` VALUES ('3', '1-C', '1', '1');
INSERT INTO `cat_grupos` VALUES ('4', '1-A', '2', '1');
INSERT INTO `cat_grupos` VALUES ('5', '1-B', '2', '1');
INSERT INTO `cat_grupos` VALUES ('6', '1-C', '0', '1');
INSERT INTO `cat_grupos` VALUES ('7', 'almarosa', '1', '1');

-- ----------------------------
-- Table structure for cat_materias
-- ----------------------------
DROP TABLE IF EXISTS `cat_materias`;
CREATE TABLE `cat_materias` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(500) DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_materia`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_materias
-- ----------------------------
INSERT INTO `cat_materias` VALUES ('1', 'INGLES', '1');
INSERT INTO `cat_materias` VALUES ('2', 'MATEMATICAS', '1');
INSERT INTO `cat_materias` VALUES ('3', 'ESPAÑOL', '1');
INSERT INTO `cat_materias` VALUES ('4', 'HISTORIA', '1');
INSERT INTO `cat_materias` VALUES ('5', 'GEOGRAFIA', '1');
INSERT INTO `cat_materias` VALUES ('6', 'QUIMICA', '1');
INSERT INTO `cat_materias` VALUES ('7', 'alma rosa', '1');
INSERT INTO `cat_materias` VALUES ('8', 'rossy', '0');
INSERT INTO `cat_materias` VALUES ('9', 'INGLESaaa', '0');
INSERT INTO `cat_materias` VALUES ('10', 'asdasd', '1');

-- ----------------------------
-- Table structure for cat_nivel_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `cat_nivel_usuarios`;
CREATE TABLE `cat_nivel_usuarios` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of cat_nivel_usuarios
-- ----------------------------
INSERT INTO `cat_nivel_usuarios` VALUES ('1', 'ADMINISTRADOR', '1');
INSERT INTO `cat_nivel_usuarios` VALUES ('2', 'DOCENTE', '1');
INSERT INTO `cat_nivel_usuarios` VALUES ('3', 'ALUMNO', '1');

-- ----------------------------
-- Table structure for r_grupo_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `r_grupo_usuarios`;
CREATE TABLE `r_grupo_usuarios` (
  `id_user_grupo` int(11) NOT NULL AUTO_INCREMENT,
  `id_grupo` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_user_grupo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_grupo_usuarios
-- ----------------------------

-- ----------------------------
-- Table structure for r_materias_usuarios
-- ----------------------------
DROP TABLE IF EXISTS `r_materias_usuarios`;
CREATE TABLE `r_materias_usuarios` (
  `id_materias_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_materia` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_materias_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of r_materias_usuarios
-- ----------------------------

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) DEFAULT NULL,
  `password` mediumtext,
  `id_nivel` int(11) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `apellido_paterno` varchar(150) DEFAULT NULL,
  `apellido_materno` varchar(150) DEFAULT NULL,
  `activo` char(1) DEFAULT '1',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('1', 'ARODRIGUEZ96', '7c4a8d09ca3762af61e59520943dc26494f8941b', '1', 'ALMA ROSA', 'RODRIGUEZ', 'SANTOS', '1');
INSERT INTO `usuarios` VALUES ('2', 'RRODRIGUEZ56', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2', 'RAUL', 'RODRIGUEZ', 'SANTOS', '1');
INSERT INTO `usuarios` VALUES ('3', 'ESANTOS23', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2', 'ELENA', 'SANTOS', 'SERRANO', '1');
INSERT INTO `usuarios` VALUES ('4', 'JPEREZ23', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2', 'JESUS ALEJANDRO', 'PEREZ', 'MARQUEZ', '1');
INSERT INTO `usuarios` VALUES ('5', 'JQUEZADA24', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3', 'JOEL', 'QUEZADA', 'GONZALEZ', '1');
INSERT INTO `usuarios` VALUES ('6', 'GCORELLA28', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3', 'GABRIEL', 'CORELLA', 'GARCIA', '1');
INSERT INTO `usuarios` VALUES ('7', 'MGUTIERREZ23', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3', 'MANUEL', 'GUTIERREZ', 'LOPEZ', '1');
INSERT INTO `usuarios` VALUES ('8', 'PMORENO42', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3', 'PEDRO', 'MORENO', 'SERRANO', '1');
INSERT INTO `usuarios` VALUES ('9', 'JVALAZUEZ67', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3', 'JULIAN', 'VELAZQUEZ ', 'CAZARES ', '1');
INSERT INTO `usuarios` VALUES ('10', 'CCORDOVA39', '7c4a8d09ca3762af61e59520943dc26494f8941b', '3', 'CESAR', 'CORDOVA', 'CALDERON ', '1');
INSERT INTO `usuarios` VALUES ('11', 'arosa17', '356a192b7913b04c54574d18c28d46e6395428ab', '1', 'alma', 'rosa', 'rodriguez', '1');
